<?php

use yii\db\Migration;

class m190412_212005_reference extends Migration
{
   //Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('reference',[
           'id' => $this->primaryKey(),
           'name' => $this->string('255'),
            'code_connect' => $this->string('255'),
            'description' => $this->text()
        ]);
    }

    public function down()
    {
        $this->dropTable('reference');
        return false;
    }

}
