<?php

use yii\db\Migration;

class m190417_212513_setting_migrat extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('setting_migrate',[
            'id' => $this->primaryKey(),
            'menu_name' => $this->string('255'),
            'db_name' => $this->string('255'),
            'field_set' => $this->integer(),
            'field_name' => $this->string('255'),
            'field_type' => $this->integer(),
            'length' => $this->integer(),
            'value' => $this->string('255'),
            'connect_model' => $this->integer(),
            'display_table' => $this->boolean(),
            'search_field' => $this->boolean(),
            'comment_field' => $this->text(),
            'dropdown_create' => $this->boolean(),
            'select_composer' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('setting_migrate');
        return false;
    }
}
