<?php

use yii\db\Migration;

class m190412_193808_project extends Migration
{
    public function up()
    {
        $this->createTable('user',[
            'id' => $this->primaryKey(),
            'setting_migrate' => $this->string('255'),
            'login' => $this->string('255'),
            'password' => $this->string('255'),
            'name' => $this->string('255'),
            'host' => $this->string('255'),
            'port' => $this->integer(),
            'company' => $this->string('255')
        ]);

        $this->insert('user',[
            'login' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('admin')
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
        return false;
    }
}
