<?php

use yii\db\Migration;

/**
 * Class m190412_211217_composer
 */
class m190412_211217_composer extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('composer',[
           'id' => $this->primaryKey(),
           'composer_id' => $this->integer(),
           'on_off' => $this->boolean()
        ]);
    }

    public function down()
    {
        $this->dropTable('composer');
        return false;
    }
}
