<?php

use yii\db\Migration;

class _migrate extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('setting_migrate',[
            'id' => $this->primaryKey(),
            'menu_name' => $this->string('255'),
            'database_name' => $this->string('255'),
            'field_set' => $this->integer(),
            'field_name' => $this->string('255'),
            'for_database_name' => $this->string('255'),
            'type_field' => $this->integer(),
            'length' => $this->string(),
            'default_value' => $this->integer(),
            'connect_model' => $this->integer(),
            'display_table' => $this->integer(),
            'search_field'  => $this->integer(),
            'comment_field' => $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('setting_migrate');
        return false;
    }
}
