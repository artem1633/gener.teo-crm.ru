<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reference".
 *
 * @property int $id
 * @property string $name
 * @property string $code_connect
 * @property string $description
 */
class Reference extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reference';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name', 'code_connect'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '',
            'name' => 'Название',
            'code_connect' => 'Код подключения',
            'description' => 'Описание',
        ];
    }
}
