<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $name
 * @property string $host
 * @property int $port
 * @property string $company
 */
class UserAdd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function getHashedPass()
    {
        return Yii::$app->getSecurity()->generatePasswordHash($this->password);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['port'], 'integer'],
            [['login', 'password', 'name', 'host', 'company','setting_migrate'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '',
            'setting_migrate' => 'Настройки базы данных ',
            'login' => 'Логи бд',
            'password' => 'Пароль бд',
            'name' => 'Название бд',
            'host' => 'Хост',
            'port' => 'Порт',
            'company' => 'Связь с компанией',
        ];
    }
}
