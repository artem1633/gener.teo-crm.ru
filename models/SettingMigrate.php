<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting_migrate".
 *
 * @property int $id
 * @property string $menu_name
 * @property string $db_name
 * @property int $field_set
 * @property string $field_name
 * @property int $field_type
 * @property string $length
 * @property string $value
 * @property string $connect_model
 * @property string $display_table
 * @property string $search_field
 * @property string $comment_field
 * @property int $dropdown_create
 * @property int $select_composer
 */
class SettingMigrate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setting_migrate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['field_set', 'field_type', 'dropdown_create', 'select_composer','display_table','length', 'search_field'], 'integer'],
            [['comment_field'], 'string'],
            [['menu_name', 'db_name', 'field_name', 'value', 'connect_model'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '',
            'menu_name' => 'Название в меню',
            'db_name' => 'Название для бд',
            'field_set' => 'Набор полей',
            'field_name' => 'Название поля',
            'field_type' => 'Тип поля',
            'length' => 'длина',
            'value' => 'Значение поумолчанию',
            'connect_model' => 'Связь с другой моделькой',
            'display_table' => 'Отоброжать в таблице',
            'search_field' => 'Поиск по полю',
            'comment_field' => 'Комментарий к полю',
            'dropdown_create' => 'Выподающий список при создании',
            'select_composer' => 'Выбор из подключеных компонентов композера',
        ];
    }
}
