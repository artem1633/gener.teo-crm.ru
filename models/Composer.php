<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "composer".
 *
 * @property int $id
 * @property int $composer_id
 * @property int $on_off
 */
class Composer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'composer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['composer_id', 'on_off'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '',
            'composer_id' => 'Связь со справочником',
            'on_off' => 'Вкл/выкл',
        ];
    }
}
