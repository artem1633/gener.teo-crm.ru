<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'User Adds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-add-index">
    <p>
        <?= Html::a('Cоздать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'login',
            //'password',
            'name',
            'host',
            'port',
            'company',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
