<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserAdd */

$this->title = 'Update User Add: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'User Adds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-add-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
