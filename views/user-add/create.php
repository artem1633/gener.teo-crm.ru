<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserAdd */

$this->title = 'Create User Add';
$this->params['breadcrumbs'][] = ['label' => 'User Adds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-add-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
