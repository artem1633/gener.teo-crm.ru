<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Reference;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Composer */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="composer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'composer_id')->dropDownList(
        ArrayHelper::map(Reference::find()->all(), 'id', 'name'),
        ['prompt' => 'Bыбрать', 'class' => 'form-control input-sm', 'id' => 'select']
    ) ?>

    <?= $form->field($model, 'on_off')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Cохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
