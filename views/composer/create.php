<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Composer */

$this->title = 'Create Composer';
$this->params['breadcrumbs'][] = ['label' => 'Composers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="composer-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
