<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Composers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="composer-index">
    <p>
        <?= Html::a('Cоздать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'composer_id',
            'on_off',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
