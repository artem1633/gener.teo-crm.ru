<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Composer */

$this->title = 'Update Composer: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Composers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="composer-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
