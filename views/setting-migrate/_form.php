<?php

use app\models\Reference;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SettingMigrate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="setting-migrate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'menu_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'db_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'field_set')->textInput() ?>

    <?= $form->field($model, 'field_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'field_type')->textInput() ?>

    <?= $form->field($model, 'length')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'connect_model')->dropDownList(
        ArrayHelper::map(app\models\SettingMigrate::find()->all(), 'id', 'menu_name'),
        ['prompt' => 'Bыбрать', 'class' => 'form-control input-sm', 'id' => 'select']
    ) ?>

    <?= $form->field($model, 'display_table')->checkbox() ?>

    <?= $form->field($model, 'search_field')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment_field')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'dropdown_create')->checkbox() ?>

    <?= $form->field($model, 'select_composer')->dropDownList(
        ArrayHelper::map(Reference::find()->all(), 'id', 'name'),
        ['prompt' => 'Bыбрать', 'class' => 'form-control input-sm', 'id' => 'select']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Cохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
