<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SettingMigrate */

$this->title = 'Create Setting Migrate';
$this->params['breadcrumbs'][] = ['label' => 'Setting Migrates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-migrate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
