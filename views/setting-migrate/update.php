<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SettingMigrate */

$this->title = 'Update Setting Migrate: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Setting Migrates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="setting-migrate-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
