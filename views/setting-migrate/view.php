<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SettingMigrate */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Setting Migrates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="setting-migrate-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Pедактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'menu_name',
            'db_name',
            'field_set',
            'field_name',
            'field_type',
            'length',
            'value',
            'connect_model',
            'display_table',
            'search_field',
            'comment_field:ntext',
            'dropdown_create',
            'select_composer',
        ],
    ]) ?>

</div>
