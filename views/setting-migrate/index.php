<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Setting Migrates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-migrate-index">
    <p>
        <?= Html::a('Cоздать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'menu_name',
            'db_name',
            'field_set',
            'field_name',
            'field_type',
            'length',
            'value',
            'connect_model',
            'display_table',
            'search_field',
            'comment_field:ntext',
            'dropdown_create',
            'select_composer',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
